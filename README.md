# Projet d'apprentissage git

## Syntaxe MarkDown

Trying to make a project for a formation 

En **gras**

_italique_
* liste
* liste
__souligner__

## Cours

faire git status (et constater)
Il regarde le fichier correspondant à object et à vu que ce n'était pas le même.
Nous allons préparer un envoie des modifications pour qu'elles soient sauvergardé en tant qu'un état particulier
Save tous les jours, tous les 15jours ou???
Il faudra faire entre les deux.
Commit  => s'engager, s'impliquer dans les choses d'un façon que l'on suppose positive. On fait un commit après les tests une fois que c'est validé
Faire des commit trop souvent necessite trop de travail(5 à 10% du temps de travail total)
A l'inverse commit trop lourd qui seront en retard, conflit et asynchronisation.
Il faut que dans le commit il se souvienne de qui à fait la modif.

## Application

Nous allons désormais faire  : **git config --global user.name etienne**

**git config --global user.email email**

**git config --global --get user.name etienne**

**git config --global --list**

**git config --local --get user.name**

**git commit -m "Duke : come get some"**

On vient de commit en vrac
ce n'est pas parce que le fichier est modifié qu'il va l'ajouter au prochain commit.
on va donc devoir lui dire que certain fichier doivent etre mis dans le prochain commit

**git add README.md**
qui permet de dire que lors du prochain commit il doit ajouter ces fichiers
git status
_stage_ signifie préparé en attente de commit
Nous avons donc un fichier suivi/tracked, modified and staged
regarder ce qu'il y a dans un repertoire

**git add rep1/** ou **git add rep**

**git add .** ou **git add ./**

**git add --all**

Pour faire un push il faut avoir les droits sur le gitlab et donc il va demander un mdp et id

**git push**

git a dit à windows ya t il un truc special gitlab?non, et on l'a créé avec l'authentification 

### Compiler


**git --add**

**git commit -m "Duke : come get some"**

**git push server2"

Pour voir son fichier

**git remote**
C'est pour voir les dépots à distance
Le dépot à distance et dans le dépot local "origine". Nous allons aller plus loin dans les infos

**git remote -v**On lit et enregistre au meme endroit donc deux adresses. Cependant, il peut y avoir d'autre dépot chacun avec une adresse les uns à côté des autres.

**git remote add serveurExemple url**

**git remote rename serveurExemple serveur2**

Cela sert à rename.

On va ensuite l'envoyer vers le serveur que l'on veut et par défaut c'était "origine" en faisant :
**git push serveur2**

Il est possible pour hacker les données on peut faire un Man in The Middle, cad faire passer les infos par notre serveur en forcant le push.

### Choisir ses fichiers

**javac calculator.java**s

**.gitignore** on peut ign

brocolis.*
ignore tous les fichiers classe
*.class
ignore cela n'importe ou, n'importe ou dans l'arborescence depuis la racine
**/choux
on les ignore

### Supprimer des fichiers

On créée un fichier par "erreur"

**notepad erreur.txt** ....enregistrer

**git add --all**

**git commit -m "erreur?"**

**git push**

Ensuite on s'occupe du fichier erreur et on le supprime avec rm --cached qui signifie removes

**git rm --cached erreur.txt**

**git status**

**git commit -m "destruction"

**git push**

#### Cas du boeing

On peut faire des commits pendant le vol mais pas des push.

nous avons 3 dépots en cours. Nous avons modifié dans le clone le rayon de la sphère et cela l'a modifié dans gitlab.
Ensuite on execute le code suivant :

**git add calculator.java**

**git commit -m "maj"**

**git push**
(vérifier dans gitlab)

**git remote**

### Les pull et fetch

#### Merge
Nous allons faire un merge

**git pull**
D'abord il fait un fetch puis un merge lorsqu'il fait le git pull

Aller chercher quelque chose. Les commandes git pull et git fetch sont toutes les deux utilisées pour mettre à jour un répertoire de travail local avec les données d'un repository distant. 

**git fetch**

Nous aurions pu faire :

**git fetch origin** == **git fetch**
Avec origin qui est le dépôt à distance de base.

**git fetch --all**

Une branche est une version. Lorsque l'on fait un commit on fabrique une nouvelle branche, branche dérivée etc
La fourche ou fork en englais permet de dérivée et de ne pas suivre la même branche. Mais il faudra se remettre d'accord pour avoir une meme version identique.
C'est pour cela que l'on fera un merge ou regroupement.

La dernière version qui est à la tête et qui a été commit sera celle qui sera prise en compte lorsque l'on fera le :

**git merge FETCH_HEAD**

La branche master est la branche principale et celle qui est disponible lrosque l'on fait :

**git branch**

On peut également obtenir plus d'infos en faisant la commande :

**git branch -v** qui nous donnera également le numéro et nom du dernier commit.(v comme verbose) sur les 32 caracteres on en prend que 7. C'est une troncature du dernier commit. 28bit = 300 millions de valeurs différentes.

Nous aurons les branches à distance avec la commande :

**git branch -v -r**

Dans le répertoire "refs" de _.git/_ on va trouver => HEADS ==> master et également =>remotes=> origin => master.
Dans le fichier master on y trouve le code du dernier commit.

Lorsque l'on fait le :

**git log**
On obtient toutes les dates nom auteur etc des commits clone qui ont été fait sur ce projet.

Changer de branche signifiera de passer d'une branche à une autre. 
Nous allons nous créer une branche americaine calculator2.

**git branch usa**

**git branch -v" Nous avons désormais deux branches dans notre projet. On voit d'ailleurs une petite étoile à côté de master qui signifie que c'est la branche en cours.

Checkout == récupérer/passer à : cela nous permet de nous positionner dans la branche

**git checkout usa**

Nous pouvons avoir un probleme de disque dur si l'on fait un checkout....

Si on change de répertoire et que l'on retourne sur le calculor on constate que la branche usa n'existe pas car il n'en connait qu'une. L'autre branche n'a pas encore été commited.
Si on va sur gilab et que l'on aime bien un projet on fait un clone et on récupère le projet. On peut faire les modif dedans direct. ou alors on fait le clone puis on part sur une autre branche pour travailler dessus.
Cela ne dérange personne et elle n'est connue que de nous.

On va devoir faire un pus special avec un upstream pour lui indiquer comment faire :

**git push -u origin usa**

**git pull --all** Permet de récupérer tout à distance ( ce qui a été fait dans une autre branche par exemple

Je change ma branche usa en lui disant que son upstream c'est origin/usa. Nous avons mis à jour le local numero 1

**git branch usa**

**git branch -u origin/usa usa**

On va faire un pull pour récupérer précisement ce qu'il faut car le all ne recupere pas tout

**git pull origin master**

je met ce que je vais chercher dans la branche en cours :

**git merge master**
